// Activity No. 1
db.users.insertMany([
	{
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "deojane@gmail.com"
		},
	courses: ["Swift", "Tailwind", "Typescript"],
	department: "HR"
	},

	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 70,
	contact: {
		phone: "09123456789",
		email: "brainsonwheels@gmail.com"
		},
	courses: ["Python", "Java", "JavaScript"],
	department: "HR"
	}
])
// Activity No. 1 END

// Activity No. 2
db.users.find(
	{
		$or: [
			{
				firstName: {$regex: 's', $options: "$i"}
			},
			{
				lastName: {$regex: 'd', $options: "$i"}
			}
	    ]
	},
	{
		firstName: 1,
		lastName: 1
	},
	{
		_id: 0
	}
)
// Activity No. 2 END

// Activity No. 3
db.users.find({
	age: {$gte: 70}
})
// Activity No. 3 END

// Activity No. 4
db.users.find(
	{
		$and: [
			{
				firstName: {$regex: 'e', $options: "$i"}
			},
			{
				age: {$lte: 30}
			}
	    ]
	}
)
// Activity No. 4 END

